package puppeteer.exception;

public class PuppeteerException extends RuntimeException {

    public PuppeteerException(String message) {
        super(message);
    }
}

[![Coverage Status](https://coveralls.io/repos/bitbucket/pipicz/puppeteer/badge.svg?branch=master)](https://coveralls.io/bitbucket/pipicz/puppeteer?branch=master)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/da9ac21224f44290974ea600e5acb33c)](https://www.codacy.com/app/istvan-pipicz/puppeteer?utm_source=pipicz@bitbucket.org&amp;utm_medium=referral&amp;utm_content=pipicz/puppeteer&amp;utm_campaign=Badge_Grade)

# Puppeteer-Framework

Puppeteer framework provides an annotation based system, where you can specify, which mark classes should be injected to which field, or constructor.\
I created it for educational purposes, and the idea is based on the [Spring Framework](https://spring.io/), however you can create your own annotations, and it's more lightweight.